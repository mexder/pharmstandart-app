# Тестовое задание для компании "Фармстандарт"

Приложение состоит из одного маршрута '/table', который выводит таблицу спаршенных данных с ресурса 'https://zakupki.gov.ru'.

## Системные требования

> - PHP 7.4
> - composer 2.3.5
> - Docker
> - docker-compose

## Начало работы

Для быстрого поднятия приложения, Вы можете воспользоваться следующей командой, если Вы используете make. (Миграции поднимаются отдельно)

```bash
$ make up
```

В другом случае, для поднятия приложения необходимо воспользоваться следующим набором команд:

#### Установка зависимостей

```bash
$ composer install
$ composer dump-autoload
$ composer development-enable
```

#### Развертывание в Docker

```bash
$ docker build -t pharmstandart-app .
```

- Unix:
```bash
$ docker-compose up -d
```
- Windows:
```bash
$ docker compose up -d
```
#### Поднятие миграция (не включено в makefile)

```bash
$ ./vendor/bin/phpmig migrate
```

После поднятия приложение, Вы можете обратиться к ресурсу 'http://localhost/table'.