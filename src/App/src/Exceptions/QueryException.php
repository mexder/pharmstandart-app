<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class QueryException
 *
 * Ошибка 500 "Query error occurred.".
 *
 * @package App\Exceptions
 */
class QueryException extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(
        string $message = "Query error occurred.",
        int $code = 500
    ) {
        parent::__construct($message, $code);
    }
}
