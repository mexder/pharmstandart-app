<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class EmptySessionDataException
 *
 * Ошибка 500 "Empty necessary session data."
 *
 * @package App\Exceptions
 */
class EmptySessionDataException extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(
        string $message = "Empty necessary session data.",
        int $code = 500
    ) {
        parent::__construct($message, $code);
    }
}
