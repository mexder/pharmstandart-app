<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class PageNotFoundException
 *
 * Ошибка 404 "Page not found."
 *
 * @package App\Exceptions
 */
class PageNotFoundException extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(
        string $message = "Page not found.",
        int $code = 404
    ) {
        parent::__construct($message, $code);
    }
}
