<?php

declare(strict_types=1);

namespace App\Contracts;

/**
 * Interface CanDataProcessing
 *
 * Интерфейс для обработки данных.
 *
 * @package App\Contracts
 */
interface CanDataProcessing
{
    /**
     * Парсинг данных
     *
     * @return void
     */
    public function processData(): void;
}
