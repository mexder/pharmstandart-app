<?php

declare(strict_types=1);

namespace App\Factories;

use App\Handlers\GetTableHandler;
use App\Models\BidListModel;
use App\Models\DocsModel;
use App\Models\GeneralInfoModel;
use App\Models\MainInfoModel;
use App\Repositories\BidListRepository;
use App\Repositories\DocsRepository;
use App\Repositories\GeneralInfoRepository;
use App\Repositories\MainInfoRepository;
use App\Services\CurlService;
use App\Services\ResponseService;
use Illuminate\Database\Capsule\Manager as Capsule;
use App\Services\DataProcessing\BidListDataProcessingService;
use App\Services\DataProcessing\DocsDataProcessingService;
use App\Services\DataProcessing\GeneralInfoDataProcessingService;
use App\Services\DataProcessing\MainInfoDataProcessingService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class GetTableHandlerFactory
 *
 * @package App\Factories
 */
class GetTableHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RequestHandlerInterface
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $config = $container->get('config');

        $baseUrl = $config['base_url'];
        $endpoints = $config['endpoints'];
        $parameters = $config['parameters'];
        $headers = $config['headers'];

        $curlService = new CurlService($headers);

        /** Подключение к бд */
        $capsule = new Capsule();
        $capsule->addConnection($container->get('config')['database']['models']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        $generalInfoRepository = new GeneralInfoRepository(new GeneralInfoModel());
        $mainInfoRepository = new MainInfoRepository(new MainInfoModel());
        $bidListRepository = new BidListRepository(new BidListModel());
        $docsRepository = new DocsRepository(new DocsModel());

        return new GetTableHandler(
            new GeneralInfoDataProcessingService(
                $baseUrl,
                $endpoints['main_info'],
                $parameters,
                $curlService,
                $generalInfoRepository
            ),
            new MainInfoDataProcessingService(
                $baseUrl,
                $endpoints['main_info'],
                $parameters,
                $curlService,
                $mainInfoRepository
            ),
            new BidListDataProcessingService(
                $baseUrl,
                $endpoints['bid_list'],
                $endpoints['bid_review'],
                $parameters,
                $curlService,
                $bidListRepository
            ),
            new DocsDataProcessingService(
                $baseUrl,
                $endpoints['docs'],
                $parameters,
                $curlService,
                $docsRepository
            ),
            new ResponseService(
                $generalInfoRepository,
                $mainInfoRepository,
                $bidListRepository,
                $docsRepository,
                $config['naming']
            )
        );
    }
}
