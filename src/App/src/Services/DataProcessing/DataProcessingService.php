<?php

declare(strict_types=1);

namespace App\Services\DataProcessing;

use App\Contracts\CanDataProcessing;
use App\Repositories\Repository;
use App\Services\CurlService;

/**
 * Class DataProcessingService
 *
 * Абтракция для сервисов по обработке данных
 */
abstract class DataProcessingService implements CanDataProcessing
{
    /** @var string $baseUrl */
    protected string $baseUrl;

    /** @var string $endpoint */
    protected string $endpoint;

    /** @var array $parameters */
    protected array $parameters;

    /** @var CurlService $curlService */
    protected CurlService $curlService;

    /** @var Repository $repository */
    protected Repository $repository;

    /**
     * @param string $baseUrl
     * @param string $endpoint
     * @param array $parameters
     * @param CurlService $curlService
     * @param Repository $repository
     */
    public function __construct(
        string $baseUrl,
        string $endpoint,
        array $parameters,
        CurlService $curlService,
        Repository $repository
    ) {
        $this->baseUrl = $baseUrl;
        $this->endpoint = $endpoint;
        $this->parameters = $parameters;
        $this->curlService = $curlService;
        $this->repository = $repository;
    }
}
