<?php

namespace App\Services\DataProcessing;

use App\Exceptions\PageNotFoundException;
use App\Exceptions\QueryException;

/**
 * Class DocsDataProcessingService
 *
 * Сервис по обработке данных со страницы protocol-main-info.html
 *
 * @package App\Services\DataProcessing
 */
class MainInfoDataProcessingService extends DataProcessingService
{
    /**
     * Парсинг данных со страницы protocol-main-info.html
     *
     * @return void
     * @throws PageNotFoundException
     * @throws QueryException
     */
    public function processData(): void
    {
        $mainInfoUrl = $this->baseUrl . $this->endpoint
            . '?regNumber=' . $this->parameters['regNumber']
            . '&protocolId=' . $this->parameters['protocolId'];

        $output = $this->curlService->getCurlOutput($mainInfoUrl);

        /** Получение поля "Наименование протокола" */
        preg_match(
            '/<span class="section__title">Наименование протокола<\/span>\s+'
            . '<span class="section__info">\s+(.*?)\s+<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('protocol_name', $matches[1]);

        /** Получение поля "Организация, осуществляющая размещение протокола" */
        preg_match(
            '/<span class="section__title">'
            . 'Организация, осуществляющая размещение протокола<\/span>\s+'
            . '<span class="section__info">\s+(.*?)<br>/s',
            $output,
            $matches
        );
        $hostingOrganization = $matches[1];

        preg_match(
            '/<a href="\/epz\/organization\/view\/info\.html\?'
            . 'organizationCode=03292000622" '
            . 'onclick="window\.open\(this\.href\); return false;" '
            . 'target="_blank">(.*?)<\/a>/s',
            $output,
            $matches
        );
        $hostingOrganization = $hostingOrganization . ' ' . $matches[1];

        $this->repository->setValue('hosting_organization', $hostingOrganization);

        /** Получение поля "Извещение о проведении электронного аукциона" */
        preg_match(
            '/<a href="\/epz\/order\/notice\/printForm\/view\.html\?'
            . 'noticeId=27772777" target="_blank">(.*?)<\/a>/s',
            $output,
            $matches
        );

        $this->repository->setValue('electronic_auction_notice', $matches[1]);

        /** Получение поля "Место подведения итогов электронного аукциона" */
        preg_match(
            '/<span class="section__title">'
            . 'Место подведения итогов электронного аукциона<\/span>\s+'
            . '<span class="section__info">\s*(.*?)\s+<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('results_summing_up_place', $matches[1]);

        /**
         * Получение полей "Дата и время составления протокола"
         * и "Дата подписания протокола"
         */
        preg_match_all(
            '/<span class="section__info">\s*(.*?)<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('protocol_drawing_up_date', preg_replace(
            "/<span class='timeZoneName' title='Москва, стандартное время'>/",
            "",
            $matches[1][4]
        ));
        $this->repository->setValue('protocol_signing_date', preg_replace(
            "/<span class='timeZoneName' title='Москва, стандартное время'>/",
            "",
            $matches[1][5]
        ));

        $this->repository->setValue('general_info_id', $_SESSION['general_info_id']);
        $this->repository->saveModel();
    }
}
