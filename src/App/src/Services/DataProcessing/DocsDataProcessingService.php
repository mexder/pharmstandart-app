<?php

namespace App\Services\DataProcessing;

use App\Exceptions\PageNotFoundException;

/**
 * Class DocsDataProcessingService
 *
 * Сервис по обработке данных со страницы protocol-docs.html
 *
 * @package App\Services\DataProcessing
 */
class DocsDataProcessingService extends DataProcessingService
{
    /**
     * Парсинг данных со страницы protocol-docs.html
     *
     * @return void
     * @throws PageNotFoundException
     */
    public function processData(): void
    {
        $docsUrl = $this->baseUrl . $this->endpoint
            . '?regNumber=' . $this->parameters['regNumber']
            . '&protocolId=' . $this->parameters['protocolId'];

        $output = $this->curlService->getCurlOutput($docsUrl);

        /** Получение блоков, хранящих информацию о документах */
        preg_match_all(
            '/<td class="tableBlock__col">(.*?)<\/td>/s',
            $output,
            $generalMatches
        );

        $dataModel = [];
        $j = 0;

        $localPathToStorage = str_replace(
            'src/App/src/Services/DataProcessing',
            '',
            __DIR__
        ) . 'public/storage/';
        $pathToStorage = 'storage/';

        for ($i = 0; $i <= count($generalMatches[1]); $i++) {
            /** Получение url для скачивания документа */
            preg_match(
                '/<a\s+href=\"(.*?)\"\s+title/s',
                $generalMatches[1][$i],
                $matches
            );

            if (empty($matches[1])) {
                continue;
            }
            $url = $matches[1];

            /** Получение имени документа */
            preg_match(
                '/title=\"(.*?)\">/s',
                $generalMatches[1][$i],
                $matches
            );
            $documentName = $matches[1];

            $output = $this->curlService
                ->getCurlOutput($url);

            file_put_contents($localPathToStorage . $documentName, $output);

            $dataModel[$j++] = [
                'file_name' => $documentName,
                'path_to_file' => $pathToStorage . $documentName,
            ];
        }

        $this->repository->createMany($dataModel);
    }
}
