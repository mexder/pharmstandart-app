<?php

namespace App\Services\DataProcessing;

use App\Exceptions\PageNotFoundException;
use App\Exceptions\QueryException;

/**
 * Class DocsDataProcessingService
 *
 * Сервис по обработке общих данных для каждой страницы
 *
 * @package App\Services\DataProcessing
 */
class GeneralInfoDataProcessingService extends DataProcessingService
{
    /**
     * Парсинг общих данных для каждой страницы
     *
     * @return void
     * @throws PageNotFoundException
     * @throws QueryException
     */
    public function processData(): void
    {
        $generalInfoUrl = $this->baseUrl . $this->endpoint
            . '?regNumber=' . $this->parameters['regNumber']
            . '&protocolId=' . $this->parameters['protocolId'];

        $output = $this->curlService->getCurlOutput($generalInfoUrl);

        /** Получение поля "Тип аукциона" */
        preg_match(
            '/<div class="cardMainInfo__title d-flex text-truncate">'
            . '\s+(.*?)\s+<span/s',
            $output,
            $matches
        );
        $auctionType = $matches[1];

        preg_match(
            '/<span class="cardMainInfo__title distancedText ml-1">'
            . '\s+(.*?)\s+(?:\S+\s+){6}<span/s',
            $output,
            $matches
        );
        $auctionType = $auctionType . ' ' . $matches[1];

        $this->repository->setValue('auction_type', $auctionType);

        /** Получение поля "Название протокола" */
        preg_match(
            '/<span class="cardMainInfo__state protocol">'
            . '(.*?)\s+(?:\S+\s+){6}<span/s',
            $output,
            $matches
        );

        $this->repository->setValue('name', $matches[1]);

        /** Получение поля "Номер закупки" */
        preg_match(
            '/<a href="\/epz\/order\/notice\/ea44\/view\/common-info\.html\?'
            . 'regNumber=0329200062221006202" target="_blank">(.*?)<\/a>/s',
            $output,
            $matches
        );

        $this->repository->setValue('purchase_number', $matches[1]);

        /** Получение поля "Объект закупки" */
        preg_match(
            '/<span class="cardMainInfo__title">Объект закупки<\/span>\s*'
            . '<span class="cardMainInfo__content">(.*?)<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('purchase_object', $matches[1]);

        /** Получение поля "Начальная цена" */
        preg_match(
            '/<span class="cardMainInfo__content cost">\s+(.*?)\s+<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('starting_price', $matches[1]);

        /** Получение поля "Размещено в ЕИС" */
        preg_match(
            '/<span class="cardMainInfo__title">Размещено в ЕИС<\/span>\s+'
            . '<span class="cardMainInfo__content">\s+(.*?)\s+<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('posted_in_EIS', $matches[1]);

        /** Получение поля "Размещено в ЭП" */
        preg_match(
            '/<span class="cardMainInfo__title">Размещено на ЭП<\/span>\s+'
            . '<span class="cardMainInfo__content">\s+(.*?)\s+<\/span>/s',
            $output,
            $matches
        );

        $this->repository->setValue('posted_in_EP', $matches[1]);

        $this->repository->saveModel();

        $_SESSION['general_info_id'] = $this->repository->getValue('id');
    }
}
