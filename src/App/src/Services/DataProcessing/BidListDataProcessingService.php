<?php

declare(strict_types=1);

namespace App\Services\DataProcessing;

use App\Exceptions\EmptySessionDataException;
use App\Exceptions\QueryException;
use App\Repositories\Repository;
use App\Services\CurlService;
use App\Exceptions\PageNotFoundException;

/**
 * Class BidListDataProcessingService
 *
 * Сервис по обработке данных со страницы protocol-bid-list.html
 *
 * @package App\Services\CurlService
 */
class BidListDataProcessingService extends DataProcessingService
{
    /** @var string Эндпоинт для имитации ajax запросов */
    private string $bidReviewEndpoint;

    /**
     * @param string $baseUrl
     * @param string $bidListEndpoint
     * @param string $bidReviewEndpoint
     * @param array $parameters
     * @param CurlService $curlService
     * @param Repository $repository
     */
    public function __construct(
        string $baseUrl,
        string $bidListEndpoint,
        string $bidReviewEndpoint,
        array $parameters,
        CurlService $curlService,
        Repository $repository
    ) {
        parent::__construct(
            $baseUrl,
            $bidListEndpoint,
            $parameters,
            $curlService,
            $repository
        );
        $this->bidReviewEndpoint = $bidReviewEndpoint;
    }

    /**
     * Парсинг данных со страницы protocol-bid-list.html
     *
     * @return void
     * @throws PageNotFoundException
     * @throws QueryException
     * @throws EmptySessionDataException
     */
    public function processData(): void
    {
        $bidListUrl = $this->baseUrl . $this->endpoint
            . '?regNumber=' . $this->parameters['regNumber']
            . '&protocolId=' . $this->parameters['protocolId'];

        $output = $this->curlService->getCurlOutput($bidListUrl);

        /**
         * Получение массива данных
         * (№, Наименование / ФИО участника, Признак допуска заявки, Порядковый номер)
         * для каждого из элементов списка заявок
         */
        preg_match_all(
            '/<td class="table__row-item normal-text">\s+(.*?)\s+<\/td>/s',
            $output,
            $generalBidListData
        );

        /**
         * Получение параметров data-lot-id и data-id
         * для последующего запроса на protocol-bid-review.html
         */
        preg_match_all('/data-lot-id=\"(.*?)\"\s+/s', $output, $dataLotId);
        preg_match_all('/data-id=\"(.*?)\">/s', $output, $dataId);

        $dataModel = [];
        $j = 0;

        for ($i = 0; $i < count($dataLotId[1]); $i++) {
            $bidReviewUrl = $this->baseUrl . $this->bidReviewEndpoint
                . '?regNumber=' . $this->parameters['regNumber']
                . '&protocolLotId=' . $dataLotId[1][$i]
                . '&protocolBidId=' . $dataId[1][$i];

            $output = $this->curlService->getCurlOutput($bidReviewUrl);

            preg_match_all(
                '/<span class="section__info">\s*(.*?)\s*<\/span>/s',
                $output,
                $matches
            );

            $dataModel[$i] = [
                'number' => $generalBidListData[1][$j++],
                'name' => $generalBidListData[1][$j++],
                'application_acceptance_sign' => $generalBidListData[1][$j++],
                'serial_number' => $generalBidListData[1][$j++],
                'type' => $matches[1][0],
                'legal_form' => $matches[1][1],
                'full_name' => $matches[1][2],
                'INN' => $matches[1][3],
                'KPP' => $matches[1][4],
                'address' => $matches[1][5],
            ];
        }

        $this->repository->createMany($dataModel);
    }
}
