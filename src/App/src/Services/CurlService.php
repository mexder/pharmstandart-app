<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\PageNotFoundException;

/**
 * Класс для работы с cURL
 *
 * @package App\Service
 */
class CurlService
{
    /** @var array $headers Заголовки, имитирующие запрос от браузера */
    private array $headers;

    /**
     * @param array $headers
     */
    public function __construct(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * Получить ответ от curl запроса
     *
     * @param string $url
     * @return string
     * @throws PageNotFoundException
     */
    public function getCurlOutput(string $url): string
    {
        $curl = curl_init($url);

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $this->headers,
        ]);
        $output = curl_exec($curl);
        curl_close($curl);

        if (!$output) {
            throw new PageNotFoundException();
        }

        return $output;
    }
}
