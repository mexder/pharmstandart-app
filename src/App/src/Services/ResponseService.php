<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\BidListRepository;
use App\Repositories\DocsRepository;
use App\Repositories\GeneralInfoRepository;
use App\Repositories\MainInfoRepository;
use App\Repositories\Repository;

/**
 * Class ResponseService
 * 
 * @package App\Services
 */
class ResponseService
{
    /** @var GeneralInfoRepository */
    private GeneralInfoRepository $generalInfoRepository;

    /** @var MainInfoRepository */
    private MainInfoRepository $mainInfoRepository;

    /** @var BidListRepository */
    private BidListRepository $bidListRepository;

    /** @var DocsRepository */
    private DocsRepository $docsRepository;

    /** @var array $naminig Наименования всех полей в таблицах */
    private array $naming;

    /**
     * @param GeneralInfoRepository $generalInfoRepository
     * @param MainInfoRepository $mainInfoRepository
     * @param BidListRepository $bidListRepository
     * @param DocsRepository $docsRepository
     * @param array $naming
     */
    public function __construct(
        GeneralInfoRepository $generalInfoRepository,
        MainInfoRepository $mainInfoRepository,
        BidListRepository $bidListRepository,
        DocsRepository $docsRepository,
        array $naming
    ) {
        $this->generalInfoRepository = $generalInfoRepository;
        $this->mainInfoRepository = $mainInfoRepository;
        $this->bidListRepository = $bidListRepository;
        $this->docsRepository = $docsRepository;
        $this->naming = $naming;
    }

    /**
     * Обработка данных для конечного ответа
     * Метод принимает шаблон html-страницы и заполняет ее данными
     *
     * @param string $data
     * @return string
     */
    public function processResponseData(string $data): string
    {
        /** Работа с таблицей "Основной раздел" */
        $data = $this->createRow(
            'general_info',
            $this->generalInfoRepository,
            '/<tbody title="general_info_tbody">(.*?)<\/tbody>/s',
            $data
        );

        /** Работа с таблицей "Общая информация о протоколе" */
        $data = $this->createRow(
            'main_info',
            $this->mainInfoRepository,
            '/<tbody title="main_info_tbody">(.*?)<\/tbody>/s',
            $data
        );

        /** Работа с таблицей "Список заявок" */
        $data = $this->createRows(
            'bid_list',
            $this->bidListRepository,
            '/<tbody title="bid_list_tbody">(.*?)<\/tbody>/s',
            $data
        );

        /** Работа с таблицей "Документы" */
        return $this->createDocsRows(
            'docs',
            $this->docsRepository,
            '/<tbody title="docs_tbody">(.*?)<\/tbody>/s',
            $data
        );
    }

    /**
     * Создать запись в таблице
     *
     * @param string $table
     * @param Repository $repository
     * @param string $pattern
     * @param string $data
     * @return string
     */
    private function createRow(
        string $table,
        Repository $repository,
        string $pattern,
        string $data
    ): string {
        $namingList = $this->naming[$table];
        $row = '<tr>';
        foreach ($namingList as $naming) {
            $row .= '<td>'
                . $repository->getValue("{$naming}")
                . '</td>';
        }
        $row .= '</tr>';

        return preg_replace(
            $pattern,
            $row,
            $data
        );
    }

    /**
     * Создать несколько записей в таблице
     *
     * @param string $table
     * @param Repository $repository
     * @param string $pattern
     * @param string $data
     * @return string
     */
    private function createRows(
        string $table,
        Repository $repository,
        string $pattern,
        string $data
    ): string {
        $namingList = $this->naming[$table];
        $values = $repository->getValues();
        $row = '';

        foreach ($values as $value) {
            $row .= '<tr>';
            foreach ($namingList as $naming) {
                $row .= '<td>'
                    . $value[$naming]
                    . '</td>';
            }
            $row .= '</tr>';
        }

        return preg_replace(
            $pattern,
            $row,
            $data
        );
    }

    /**
     * Создать несколько записей в таблице "Документы"
     *
     * @param string $table
     * @param Repository $repository
     * @param string $pattern
     * @param string $data
     * @return string
     */
    private function createDocsRows(
        string $table,
        Repository $repository,
        string $pattern,
        string $data
    ): string {
        $namingList = $this->naming[$table];
        $values = $repository->getValues();
        $row = '';

        foreach ($values as $value) {
            $row .= '<tr>';
            $row .= "<td><a href=\""
                . $value[$namingList[1]]
                . "\" download>"
                . $value[$namingList[0]]
                . '</a></td>';
            $row .= '</tr>';
        }

        return preg_replace(
            $pattern,
            $row,
            $data
        );
    }
}
