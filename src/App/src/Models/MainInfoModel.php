<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MainInfoModel
 *
 * Модель таблицы main_info
 *
 * @property mixed $id
 * @property mixed $protocol_name Наименование протокола
 * @property mixed $hosting_organization Организация, осуществляющая размещение протокола
 * @property mixed $electronic_auction_notice Извещение о проведении электронного аукциона
 * @property mixed $results_summing_up_place Место подведения итогов электронного аукциона
 * @property mixed $protocol_drawing_up_date Дата и время составления протокола
 * @property mixed $protocol_signing_date Дата подписания протокола
 *
 * @package App\Models
 */
class MainInfoModel extends Model
{
    /** @var string $table */
    protected $table = 'main_info';

    /** @var array $guarded */
    protected $guarded = [
        'protocol_name',
        'hosting_organization',
        'electronic_auction_notice',
        'results_summing_up_place',
        'protocol_drawing_up_date',
        'protocol_signing_date',
    ];

    /**
     * GeneralInfoModel - MainInfoModel
     * Связь один к одному
     *
     * @return BelongsTo
     */
    public function generalInfo(): BelongsTo
    {
        return self::belongsTo(GeneralInfoModel::class);
    }
}
