<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class BidListModel
 *
 * Модель таблицы bid_list
 *
 * @property mixed $id
 * @property mixed $number №
 * @property mixed $name Наименование / ФИО участника
 * @property mixed $application_acceptance_sign Признак допуска заявки
 * @property mixed $serial_number Порядковый номер
 * @property mixed $type Вид
 * @property mixed $legal_form Организационно-правовая форма
 * @property mixed $full_name Полное наименование
 * @property mixed $INN ИНН
 * @property mixed $KPP КПП
 * @property mixed $address Почтовый адрес
 *
 * @package App\Models
 */
class BidListModel extends Model
{
    /** @var string $table */
   protected $table = 'bid_list';

   /** @var array $fillable */
   protected $fillable = [
       'general_info_id',
       'number',
       'name',
       'application_acceptance_sign',
       'serial_number',
       'type',
       'legal_form',
       'full_name',
       'INN',
       'KPP',
       'address',
   ];

    /**
     * GeneralInfoModel - BidListModel
     * Связь один ко многим
     *
     * @return BelongsTo
     */
   public function generalInfo(): BelongsTo
   {
       return self::belongsTo(GeneralInfoModel::class);
   }
}
