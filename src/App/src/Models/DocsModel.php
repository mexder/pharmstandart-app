<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class DocsModel
 *
 * Модель таблицы docs
 *
 * @property mixed $id
 * @property mixed $file_name Наименование файла
 * @property mixed $path_to_file Путь к фалу
 *
 * @package App\Models
 */
class DocsModel extends Model
{
    /** @var string $table */
    protected $table = 'docs';

    /** @var array $fillable */
    protected $fillable = [
        'file_name',
        'path_to_file',
    ];

    /**
     * GeneralInfoModel - DocsModel
     * Связь один ко многим
     *
     * @return BelongsTo
     */
    public function generalInfo(): BelongsTo
    {
        return self::belongsTo(GeneralInfoModel::class);
    }
}
