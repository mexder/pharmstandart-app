<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class GeneralInfoModel
 *
 * Модель таблицы general_info
 *
 * @property mixed $id
 * @property mixed $auction_type Тип аукциона
 * @property mixed $name Название протокола
 * @property mixed $purchase_number Номер закупки
 * @property mixed $purchase_object Объект закупки
 * @property mixed $starting_price Начальная цена
 * @property mixed $posted_in_EIS Размещено в ЕИС
 * @property mixed $posted_in_EP Размещено в ЭП
 *
 * @package App\Models
 */
class GeneralInfoModel extends Model
{
    /** @var string $table */
    protected $table = 'general_info';

    /** @var array $guarded */
    protected $guarded = [
        'auction_type',
        'name',
        'purchase_number',
        'purchase_object',
        'starting_price',
        'posted_in_EIS',
        'posted_in_EP',
    ];

    /**
     * GeneralInfoModel - MainInfoModel
     * Связь один к одному
     *
     * @return HasOne
     */
    public function mainInfo(): HasOne
    {
        return self::hasOne(MainInfoModel::class, 'general_info_id');
    }

    /**
     * GeneralInfoModel - BidListModel
     * Связь один ко многим
     *
     * @return HasMany
     */
    public function bidLists(): HasMany
    {
        return self::hasMany(BidListModel::class, 'general_info_id');
    }

    /**
     * GeneralInfoModel - DocsModel
     * Связь один ко многим
     *
     * @return HasMany
     */
    public function docs(): HasMany
    {
        return self::hasMany(DocsModel::class, 'general_info_id');
    }
}
