<?php

declare(strict_types=1);

namespace App\Handlers;

use App\Exceptions\EmptySessionDataException;
use App\Exceptions\PageNotFoundException;
use App\Exceptions\QueryException;
use App\Services\DataProcessing\BidListDataProcessingService;
use App\Services\DataProcessing\DocsDataProcessingService;
use App\Services\DataProcessing\GeneralInfoDataProcessingService;
use App\Services\DataProcessing\MainInfoDataProcessingService;
use App\Services\ResponseService;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

/**
 * Class GetTableHandler
 *
 * Обрабатывает запрос по маршруту /table
 *
 * @package App\Handlers
 */
class GetTableHandler implements RequestHandlerInterface
{
    /** @var GeneralInfoDataProcessingService */
    private GeneralInfoDataProcessingService $generalInfoDataProcessingService;

    /** @var MainInfoDataProcessingService */
    private MainInfoDataProcessingService $mainInfoDataProcessingService;

    /** @var BidListDataProcessingService */
    private BidListDataProcessingService $bidListDataProcessingService;

    /** @var DocsDataProcessingService */
    private DocsDataProcessingService $docsDataProcessingService;

    /** @var ResponseService  */
    private ResponseService $responseService;

    private const PATH_TO_TEMPLATE = __DIR__ . '/../../../../templates/app/table.phtml';
    /**
     * @param GeneralInfoDataProcessingService $generalInfoDataProcessingService
     * @param MainInfoDataProcessingService $mainInfoDataProcessingService
     * @param BidListDataProcessingService $bidListDataProcessingService
     * @param DocsDataProcessingService $docsDataProcessingService
     * @param ResponseService $responseService
     */
    public function __construct(
        GeneralInfoDataProcessingService $generalInfoDataProcessingService,
        MainInfoDataProcessingService $mainInfoDataProcessingService,
        BidListDataProcessingService $bidListDataProcessingService,
        DocsDataProcessingService $docsDataProcessingService,
        ResponseService $responseService
    ) {
        $this->generalInfoDataProcessingService =$generalInfoDataProcessingService;
        $this->mainInfoDataProcessingService = $mainInfoDataProcessingService;
        $this->bidListDataProcessingService = $bidListDataProcessingService;
        $this->docsDataProcessingService = $docsDataProcessingService;
        $this->responseService = $responseService;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->generalInfoDataProcessingService->processData();
            $this->mainInfoDataProcessingService->processData();
            $this->bidListDataProcessingService->processData();
            $this->docsDataProcessingService->processData();
        } catch (
            PageNotFoundException
            | QueryException
            | EmptySessionDataException $exception
        ) {
            return new HtmlResponse($exception->getMessage());
        } catch (Throwable $throwable) {
            return new HtmlResponse('Undefined error.');
        }

        $html = file_get_contents(self::PATH_TO_TEMPLATE);

        return new HtmlResponse($this->responseService->processResponseData($html));
    }
}
