<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Exceptions\EmptySessionDataException;
use App\Exceptions\QueryException;
use App\Models\GeneralInfoModel;
use Error;

/**
 * Class DocsRepository
 *
 * Репозиторий для работы с моделью DocsModel
 *
 * @package App\Repositories
 */
class DocsRepository extends Repository
{
    /**
     * Создание нескольких записей в таблице
     * @param array $value
     * @return void
     * @throws EmptySessionDataException
     * @throws QueryException
     */
    public function createMany(array $value): void
    {
        if (empty($_SESSION['general_info_id'])) {
            throw new EmptySessionDataException();
        }

        $generalInfo = GeneralInfoModel::find($_SESSION['general_info_id']);

        try {
            $generalInfo->docs()->createMany($value);
        } catch (Error $error) {
            throw new QueryException();
        }
    }

    /**
     * Получить массив значений
     *
     * @return array | null
     * @throws EmptySessionDataException
     * @throws QueryException
     */
    public function getValues(): ?array
    {
        if (empty($_SESSION['general_info_id'])) {
            throw new EmptySessionDataException();
        }

        $generalInfo = GeneralInfoModel::find($_SESSION['general_info_id']);

        try {
            $docsCollection = $generalInfo->docs;

            if (is_null($docsCollection)) {
                return $docsCollection;
            }

            $docsAttributes = [];
            $i = 0;
            foreach ($docsCollection as $doc) {
                $docsAttributes[$i++] = $doc->getAttributes();
            }

            return $docsAttributes;
        } catch (Error $error) {
            throw new QueryException();
        }
    }
}
