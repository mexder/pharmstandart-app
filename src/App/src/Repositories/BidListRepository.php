<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Exceptions\EmptySessionDataException;
use App\Exceptions\QueryException;
use App\Models\GeneralInfoModel;
use Error;

/**
 * Class BidListRepository
 *
 * Репозиторий для работы с моделью BidListModel
 *
 * @package App\Repositories
 */
class BidListRepository extends Repository
{
    /**
     * Создание нескольких записей в таблице
     * @param array $value
     * @return void
     * @throws EmptySessionDataException
     * @throws QueryException
     */
    public function createMany(array $value): void
    {
        if (empty($_SESSION['general_info_id'])) {
            throw new EmptySessionDataException();
        }

        $generalInfo = GeneralInfoModel::find($_SESSION['general_info_id']);

        try {
            $generalInfo->bidLists()->createMany($value);
        } catch (Error $error) {
            throw new QueryException();
        }
    }

    /**
     * Получить массив значений
     *
     * @return array | null
     * @throws EmptySessionDataException
     * @throws QueryException
     */
    public function getValues(): ?array
    {
        if (empty($_SESSION['general_info_id'])) {
            throw new EmptySessionDataException();
        }

        $generalInfo = GeneralInfoModel::find($_SESSION['general_info_id']);

        try {
            $bidListCollection = $generalInfo->bidLists;

            if (is_null($bidListCollection)) {
                return $bidListCollection;
            }

            $bidListAttributes = [];
            $i = 0;
            foreach ($bidListCollection as $bidList) {
                $bidListAttributes[$i++] = $bidList->getAttributes();
            }

            return $bidListAttributes;
        } catch (Error $error) {
            throw new QueryException();
        }
    }
}
