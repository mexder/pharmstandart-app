<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Exceptions\QueryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\QueryException as EloquentQueryException;

/**
 * Class Repository
 *
 * Абстрактный класс репозитория для работы с моделями
 *
 * @package App\Repositories
 */
abstract class Repository
{
    /** @var Model $model */
    protected Model $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Записать данные в поле
     *
     * @param string $column
     * @param mixed $value
     * @return void
     */
    public function setValue(string $column, $value): void
    {
        if (Capsule::schema()->hasColumn($this->model->getTable(), $column)) {
            $this->model->$column = $value;
        }
    }

    /**
     * Получить значение поля
     *
     * @param string $column
     * @return mixed|null
     */
    public function getValue(string $column)
    {
        if (Capsule::schema()->hasColumn($this->model->getTable(), $column)) {
            return $this->model->$column;
        }

        return null;
    }

    /**
     * Сохранить запись в БД
     *
     * @return void
     * @throws QueryException
     */
    public function saveModel(): void
    {
        try {
            $this->model->save();
        } catch (EloquentQueryException $exception) {
            throw new QueryException();
        }
    }
}
