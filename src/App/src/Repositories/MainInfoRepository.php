<?php

declare(strict_types=1);

namespace App\Repositories;

/**
 * Class MainInfoRepository
 *
 * Репозиторий для работы с моделью MainInfoModel
 *
 * @package App\Repositories
 */
class MainInfoRepository extends Repository
{
}
