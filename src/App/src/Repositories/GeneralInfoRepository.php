<?php

declare(strict_types=1);

namespace App\Repositories;

/**
 * Class GeneralInfoRepository
 *
 * Репозиторий для работы с моделью GeneralInfoModel
 *
 * @package App\Repositories
 */
class GeneralInfoRepository extends Repository
{
}
