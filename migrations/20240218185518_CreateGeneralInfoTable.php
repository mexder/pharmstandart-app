<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

/**
 * Migration CreateGeneralProtocolInfoTable
 */
class CreateGeneralInfoTable extends Migration
{
    /**
     * @var string $table Название таблицы, с которой работает миграция
     */
    private string $table = 'general_info';

    /**
     * Поднять миграцию для создания таблицы general_protocol_info
     * (если она не была поднята ранее)
     *
     * @return void
     */
    public function up(): void
    {
        Capsule::schema()->create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('auction_type');
            $table->string('name');
            $table->string('purchase_number');
            $table->string('purchase_object');
            $table->string('starting_price');
            $table->string('posted_in_EIS');
            $table->string('posted_in_EP');
            $table->timestamps();
        });
    }

    /**
     * Откатить миграцию для создания таблицы general_protocol_info
     * (удалить табоицу)
     *
     * @return void
     */
    public function down(): void
    {
        Capsule::schema()->drop($this->table);
    }
}
