<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

/**
 * Migration CreateMainInfoTable
 */
class CreateMainInfoTable extends Migration
{
    /**
     * @var string $table Название таблицы, с которой работает миграция
     */
    private string $table = 'main_info';

    /**
     * Поднять миграцию для создания таблицы main_info
     * (если она не была поднята ранее)
     *
     * @return void
     */
    public function up(): void
    {
        Capsule::schema()->create($this->table, function (Blueprint $table) {
            $table->id();
            $table->foreignId('general_info_id');
            $table->string('protocol_name');
            $table->string('hosting_organization');
            $table->string('electronic_auction_notice');
            $table->string('results_summing_up_place');
            $table->string('protocol_drawing_up_date');
            $table->string('protocol_signing_date');
            $table->timestamps();
        });
    }

    /**
     * Откатить миграцию для создания таблицы main_info
     * (удалить табоицу)
     *
     * @return void
     */
    public function down(): void
    {
        Capsule::schema()->drop($this->table);
    }
}
