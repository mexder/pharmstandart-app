<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

/**
 * Migration CreateDocsTable
 */
class CreateDocsTable extends Migration
{
    /**
     * @var string $table Название таблицы, с которой работает миграция
     */
    private string $table = 'docs';

    /**
     * Поднять миграцию для создания таблицы docs
     * (если она не была поднята ранее)
     *
     * @return void
     */
    public function up(): void
    {
        Capsule::schema()->create($this->table, function (Blueprint $table) {
            $table->id();
            $table->foreignId('general_info_id');
            $table->string('file_name');
            $table->string('path_to_file');
            $table->timestamps();
        });
    }

    /**
     * Откатить миграцию для создания таблицы docs
     * (удалить табоицу)
     *
     * @return void
     */
    public function down(): void
    {
        Capsule::schema()->drop($this->table);
    }
}
