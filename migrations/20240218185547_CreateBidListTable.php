<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

/**
 * Migration CreateBidListTable
 */
class CreateBidListTable extends Migration
{
    /**
     * @var string $table Название таблицы, с которой работает миграция
     */
    private string $table = 'bid_list';

    /**
     * Поднять миграцию для создания таблицы bid_list
     * (если она не была поднята ранее)
     *
     * @return void
     */
    public function up(): void
    {
        Capsule::schema()->create($this->table, function (Blueprint $table) {
            $table->id();
            $table->foreignId('general_info_id');
            $table->string('number');
            $table->string('name');
            $table->string('application_acceptance_sign');
            $table->string('serial_number');
            $table->string('type');
            $table->string('legal_form');
            $table->string('full_name');
            $table->string('INN');
            $table->string('KPP');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Откатить миграцию для создания таблицы bid_list
     * (удалить табоицу)
     *
     * @return void
     */
    public function down(): void
    {
        Capsule::schema()->drop($this->table);
    }
}
