<?php

declare(strict_types=1);

return [
    'base_url' => 'https://zakupki.gov.ru/epz/order/notice/ea44/view/protocol/',
    'endpoints' => [
        'main_info' => 'protocol-main-info.html',
        'bid_list' => 'protocol-bid-list.html',
        'bid_review' => 'protocol-bid-review.html',
        'docs' => 'protocol-docs.html',
    ],
    'parameters' => [
        'regNumber' => '0329200062221006202',
        'protocolId' => '35530565',
    ],
    'headers' => [
        'cache-control: max-age=0',
        'upgrade-insecure-requests: 1',
        'user-agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
        'sec-fetch-user: ?1',
        'accept: text/html,application/xhtml+xml,application/xml;q-0.9,image/webp,image/apng,*/*;q-0.8,application/signed-exchange;v-b3',
        'x-compress: null',
        'sec-fetch-site: none',
        'sec-fetch-mode: navigate',
        'accept-encoding: default, br',
        'accept-language: ru-RU,ru;q-0.9,en-US;q-0.8,en;q-0.',
    ],
];
