<?php

declare(strict_types=1);

return [
    'naming' => [
        'general_info' => [
            'auction_type',
            'name',
            'purchase_number',
            'purchase_object',
            'starting_price',
            'posted_in_EIS',
            'posted_in_EP',
        ],
        'main_info' => [
            'protocol_name',
            'hosting_organization',
            'electronic_auction_notice',
            'results_summing_up_place',
            'protocol_drawing_up_date',
            'protocol_signing_date',
        ],
        'bid_list' => [
            'number',
            'name',
            'application_acceptance_sign',
            'serial_number',
            'type',
            'legal_form',
            'full_name',
            'INN',
            'KPP',
            'address',
        ],
        'docs' => [
            'file_name',
            'path_to_file',
        ],
    ],
];