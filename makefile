up: install
	docker build -t pharmstandart-app .
	docker-compose up -d

install:
	composer install
	composer dump-autoload
	composer development-enable
